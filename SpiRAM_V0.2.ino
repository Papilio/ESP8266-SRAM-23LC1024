 /***************************************************************************************
 *  Vorlage:                                                                            *
 *  Arduino IDE ESP8266 - library for SPI Serial SRam 23A1024/23LC1024                  *
 *  https://github.com/Gianbacchio/ESP8266_Spiram                                       *
 *                                                                                      *
 *  Arduino IDE 1.8.19                                                                  *
 *  ESP8266-Boards (3.0.2), Board: "LOLIN(WeMos)D1 R1"                                  * 
 *  Pin-Belegung wie in der Vorlage. Keine Widerstände etc. Vcc 5V                      *
 *  Der Speicher ist flüchtig, Batteriepuffer zur Datensicherung notwendig.             *
 *  Laut Datenblatt: RAM Data Retention Voltage: 1.0                                    *
 *  Aber: "This is the limit to which VCC can be lowered without losing RAM data.       *
 *  This parameter is periodically sampled and not 100% tested.                         *                   *
 *  Habe eine AA-Zelle mit 1,36 V über eine Schutzdiode (1N5817) an Vcc                 * 
 *  angeschlossen. Spannung an Vcc (5 V Versorgung getrennt) 1,09 V. Daten sicher.      *
 *  Während der Messung des Stromes (3,5 mA) waren es noch 0,89 V an Vcc. Daten sicher. *
 *  *************************************************************************************
 *  Kleine Spielerei mit dem SRam-Speicher 23LC1024  
 *  "ESP8266Spiram Spiram;" (Zeile 18) löschen oder auskommentieren.
 ****************************************************************************************
 *  ESP8266-SRAM 23LC1024
    Copyright (C) 2022  Papilio

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 */
#include <SPI.h>
#include <ESP8266Spiram.h>

//ESP8266Spiram Spiram;
// Musste ich wegen Fehler beim Kompilieren auskommentieren:
// ... libraries/ESP8266_Spiram-master/ESP8266Spiram.cpp.o:(.bss.Spiram+0x0): multiple definition of `Spiram'...

void setup() {
  Serial.begin(115200);
  delay(1000);
  Spiram.begin();
}
void loop() {
   uint8_t Dat1[9] = "{erste }"; // "{}", "[]", "()" haben keine funktionale Bedeutung; nur Symbolbeispiele.
    Spiram.write(0x09, Dat1, 9);        // "0x09" Speicherplatz ab 09
    Spiram.read(0x09, Dat1, 9);
     
   uint8_t Dat2[9] = "[zweite]"; 
    Spiram.write(0x18, Dat2, 9);        // "0x18" Speicherplatz ab 18 
    Spiram.read(0x18, Dat2, 9);
     
   uint8_t Dat3[8] = "( Zahl)";
    Spiram.write(0x23, Dat3, 8);        // "0x18" Speicherplatz ab 23
    Spiram.read(0x23, Dat3, 8); 
      Serial.print("Text: ");
      
   for (int i = 0; i < 8; i++)
      {
       char c = (Dat1)[i];
       Serial.print(c);
      }
      
   for (int i = 0; i < 8; i++)
      {
       char d = (Dat2)[i];
       Serial.print(d);
      }
           
   for (int i = 0; i < 7; i++)
      {
       char e = (Dat3)[i];
       Serial.print(e);
      }    
 
  delay(2000);
  Serial.println("");
}
